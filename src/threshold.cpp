/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "threshold.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define UNKNOWN 127

#define DO_FILL false



// very very simple fixed cutoff threshold on whole image
uint8_t mcv_threshold_fixed(const int w, const int h, const uint8_t *im, uint8_t *threshim, uint8_t cutoff){

    for(int i = 0; i < w*h; i++){
        threshim[i] = (im[i] > cutoff ? 255 : 0);
    }
    return cutoff;
}

// The idea is to find the maximum and minimum values in a
// window around each pixel. If it's a contrast-free region
// (max-min is small), don't try to binarize. Otherwise,
// threshold according to (max+min)/2.
//
// Mark low-contrast regions with value UNKNOWN so that we can skip
// future work on these areas too.

// however, computing max/min around every pixel is needlessly
// expensive. We compute max/min for tiles. To avoid artifacts
// that arise when high-contrast features appear near a tile
// edge (and thus moving from one tile to another results in a
// large change in max/min value), the max/min values used for
// any pixel are computed from all 3x3 surrounding tiles. Thus,
// the max/min sampling area for nearby pixels overlap by at least
// one tile.
//
// The important thing is that the windows be large enough to
// capture edge transitions; the tag does not need to fit into
// a tile.

// XXX Tunable. Generally, small tile sizes--- so long as they're
// large enough to span a single tag edge--- seem to be a winner.
constexpr int tilesz = 40;

// tuneable value
constexpr int min_white_black_diff = 40;

void mcv_threshold_tile(const int w, const int h, const uint8_t *im, uint8_t *threshim)
{

    // the last (possibly partial) tiles along each row and column will
    // just use the min/max value from the last full tile.
    int tw = w / tilesz;
    int th = h / tilesz;

    uint8_t *im_max = (uint8_t *)calloc(tw*th, sizeof(uint8_t));
    uint8_t *im_min = (uint8_t *)calloc(tw*th, sizeof(uint8_t));

    // first, collect min/max statistics for each tile
    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {
            uint8_t max = 0, min = 255;

            for (int dy = 0; dy < tilesz; dy++) {

                for (int dx = 0; dx < tilesz; dx++) {

                    uint8_t v = im[(ty*tilesz+dy)*w + tx*tilesz + dx];
                    if (v < min)
                        min = v;
                    if (v > max)
                        max = v;
                }
            }

            im_max[ty*tw+tx] = max;
            im_min[ty*tw+tx] = min;
        }
    }

    // second, apply 3x3 max/min convolution to "blur" these values
    // over larger areas. This reduces artifacts due to abrupt changes
    // in the threshold value.
    uint8_t *im_max_tmp = (uint8_t *)calloc(tw*th, sizeof(uint8_t));
    uint8_t *im_min_tmp = (uint8_t *)calloc(tw*th, sizeof(uint8_t));

    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {
            uint8_t max = 0, min = 255;

            for (int dy = -1; dy <= 1; dy++) {
                if (ty+dy < 0 || ty+dy >= th)
                    continue;
                for (int dx = -1; dx <= 1; dx++) {
                    if (tx+dx < 0 || tx+dx >= tw)
                        continue;

                    uint8_t m = im_max[(ty+dy)*tw+tx+dx];
                    if (m > max)
                        max = m;
                    m = im_min[(ty+dy)*tw+tx+dx];
                    if (m < min)
                        min = m;
                }
            }

            im_max_tmp[ty*tw + tx] = max;
            im_min_tmp[ty*tw + tx] = min;
        }
    }
    free(im_max);
    free(im_min);
    im_max = im_max_tmp;
    im_min = im_min_tmp;

    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {

            int min = im_min[ty*tw + tx];
            int max = im_max[ty*tw + tx];

            // low contrast region? (no edges)
            if (max - min < min_white_black_diff) {
                for (int dy = 0; dy < tilesz; dy++) {
                    int y = ty*tilesz + dy;

                    for (int dx = 0; dx < tilesz; dx++) {
                        int x = tx*tilesz + dx;

                        threshim[y*w+x] = UNKNOWN;
                    }
                }
                continue;
            }

            // otherwise, actually threshold this tile.

            // argument for biasing towards dark; specular highlights
            // can be substantially brighter than white tag parts
            uint8_t thresh = min + (max - min) / 2;

            for (int dy = 0; dy < tilesz; dy++) {
                int y = ty*tilesz + dy;

                for (int dx = 0; dx < tilesz; dx++) {
                    int x = tx*tilesz + dx;

                    uint8_t v = im[y*w+x];
                    if (v > thresh)
                        threshim[y*w+x] = 255;
                    else
                        threshim[y*w+x] = 0;
                }
            }
        }
    }

    // we skipped over the non-full-sized tiles above. Fix those now.
    for (int y = 0; y < h; y++) {

        // what is the first x coordinate we need to process in this row?

        int x0;

        if (y >= th*tilesz) {
            x0 = 0; // we're at the bottom; do the whole row.
        } else {
            x0 = tw*tilesz; // we only need to do the right most part.
        }

        // compute tile coordinates and clamp.
        int ty = y / tilesz;
        if (ty >= th)
            ty = th - 1;

        for (int x = x0; x < w; x++) {
            int tx = x / tilesz;
            if (tx >= tw)
                tx = tw - 1;

            int max = im_max[ty*tw + tx];
            int min = im_min[ty*tw + tx];
            int thresh = min + (max - min) / 2;

            uint8_t v = im[y*w+x];
            if (v > thresh)
                threshim[y*w+x] = 255;
            else
                threshim[y*w+x] = 0;
        }
    }

    free(im_min);
    free(im_max);


    if(DO_FILL){
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int val = y*w+x;

                if(val != 0 && threshim[val] == UNKNOWN){
                    if(x == 0){
                        threshim[val] = 0;
                    }else{
                        threshim[val] = threshim[val-1];
                    }
                }

            }
        }
    }

}

void mcv_threshold_tile_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    // 3x4 grid of tiles
    const int tw = 4;
    const int th = 3;

    // calc tile edge lengths
    int tiles_x =  w/tw;
    int tiles_y = h/th;

    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

    uint8_t *im_avg = (uint8_t *)calloc(tw*th, sizeof(uint8_t));

    // first, collect avg pixel val for each tile
    // start at x, y and iterate over the w/h we just calculated
    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {
            int curr_avg = 0;
            for (int dy = 0; dy < tiles_y; dy++) {
                for (int dx = 0; dx < tiles_x; dx++) {
                    uint8_t v = im[(ty*tiles_y+dy+y)*im_w + tx*tiles_x + dx + x];
                    curr_avg += v;
                }
            }
            im_avg[ty*tw+tx] = (curr_avg * tw * th)/(w*h);
        }
    }

    // second, apply 3x3 max/min convolution to "blur" these values
    // over larger areas. This reduces artifacts due to abrupt changes
    // in the threshold value.
    uint8_t *im_avg_tmp = (uint8_t *)calloc(tw*th, sizeof(uint8_t));

    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {
            uint8_t min_avg = 255; // try to find the lowest thresh value

            for (int dy = -1; dy <= 1; dy++) {
                if (ty+dy < 0 || ty+dy >= th)
                    continue;
                for (int dx = -1; dx <= 1; dx++) {
                    if (tx+dx < 0 || tx+dx >= tw)
                        continue;

                    uint8_t m = im_avg[(ty+dy)*tw+tx+dx];
                    if (m < min_avg)
                        min_avg = m;
                }
            }
            // split the difference towards the lower thresh value of its neighbors
            if (im_avg[ty*tw + tx] > min_avg)
                im_avg_tmp[ty*tw + tx] = im_avg[ty*tw + tx] - ((im_avg[ty*tw + tx] - min_avg)/2);
            // retain min otherwise
            else im_avg_tmp[ty*tw + tx] = min_avg;
        }
    }

    free(im_avg);
    im_avg = im_avg_tmp;

    for (int ty = 0; ty < th; ty++) {
        for (int tx = 0; tx < tw; tx++) {
            // argument for biasing towards dark; specular highlights
            // can be substantially brighter than white tag parts
            uint8_t thresh = im_avg[ty*tw + tx];
            if(((int)thresh + (int)adjustment)>240){
                thresh = 240;
            }
            else if(((int)thresh + (int)adjustment)<15){
                thresh = 15;
            }
            else{
                thresh += adjustment;
            }

            for (int dy = 0; dy < tiles_y; dy++) {
                int _y = (ty*tiles_y + dy + y) * im_w;

                for (int dx = 0; dx < tiles_x; dx++) {
                    int _x = tx*tiles_x + dx + x;

                    uint8_t v = im[_y+_x]; 
                    if (v > thresh){
                        threshim[_y+_x] = 255;
                    }
                    else {
                        threshim[_y+_x] = 0;
                    }
                }
            }
        }
    }
    free(im_avg_tmp);
}


uint8_t mcv_threshold_mean_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

    int im_average = 0;
    static int im_pixels = w*h;

    // first, collect average val of entire section
    for (int ty = y; ty < y+h; ty++) {
        for (int tx = x; tx < x+w; tx++) {
            uint8_t v = im[(ty*im_w) + tx];
            im_average += v;
        }
    }
    im_average /= im_pixels;

    uint8_t thresh;

    if((im_average + (int)adjustment)>240){
        thresh = 240;
    }
    else if((im_average + (int)adjustment)<15){
        thresh = 15;
    }
    else{
        thresh = im_average + adjustment;
    }

    for (int ty = y; ty < y+h; ty++) {
        for (int tx = x; tx < x+w; tx++) {

            uint8_t v = im[(ty*im_w) +tx];
            if (v >= thresh){
                threshim[(ty*im_w) +tx] = 255;
            }
            else {
                threshim[(ty*im_w) +tx] = 0;
            }
        }
    }
    return thresh;
}




uint8_t mcv_threshold_median_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

    int bins[256];
    memset(bins, 0, 256*sizeof(int));

    static int im_pixels = w*h;

    // first, bin the entire section
    for (int ty = y; ty < y+h; ty++) {
        for (int tx = x; tx < x+w; tx++) {
            uint8_t v = im[(ty*im_w) + tx];
            bins[v]++;
        }
    }

    // now find the median
    int median;
    int sum = 0;
    for(median=0;median<255;median++){
        sum+=bins[median];
        if(sum>=(im_pixels/2)) break;
    }

    // safely add the adjustment to find the threshold
    uint8_t thresh;
    if((median + (int)adjustment)>240){
        thresh = 240;
    }else if((median + (int)adjustment)<15){
        thresh = 15;
    }else{
        thresh = median + adjustment;
    }

    for (int ty = y; ty < y+h; ty++) {
        for (int tx = x; tx < x+w; tx++) {
            uint8_t v = im[(ty*im_w) +tx];
            if (v >= thresh){
                threshim[(ty*im_w) +tx] = 255;
            }else {
                threshim[(ty*im_w) +tx] = 0;
            }
        }
    }
    return thresh;
}




static uint8_t _calc_thresh_otsu_partial( int im_w, int im_h, int x, int y, \
                                    int w, int h, const uint8_t *im, int adjustment)
{
    int bins[256];
    memset(bins, 0, 256*sizeof(int));
    int sum = 0;

    static int im_pixels = w*h;
    int x_end = x+w;
    int y_end = y+h;

    // sanity check bounds on the box
    if(x_end>im_w) x_end = im_w;
    if(y_end>im_h) y_end = im_h;

    // first, bin the entire section and find the mean
    for (int ty = y; ty < y_end; ty++) {
        for (int tx = x; tx < x_end; tx++) {
            uint8_t v = im[(ty*im_w) + tx];
            bins[v]++;
            sum+=v;
        }
    }

    // now use otsu's method to find the cutoff, limit to 10 itterations
    int prediction = sum/im_pixels;
    int i;
    for(i=0;i<10;i++){

        //printf("i: %2d pred: %3d\n", i, prediction);

        int avg1 = 0;
        int n1 = 0;
        for(int j=0; j<prediction; j++){
            avg1 += (j*bins[j]);
            n1 += bins[j];
        }
        avg1 /= n1;

        int avg2 = 0;
        int n2 = 0;
        for(int j=prediction; j<=255; j++){
            avg2 += (j*bins[j]);
            n2 += bins[j];
        }
        avg2 /= n2;

        int old_prediction = prediction;
        prediction = (avg1 + avg2)/2;

        // quit now if we are close enough
        if(abs(old_prediction - prediction)<=2) break;
    }

    //printf("i: %2d pred: %3d\n\n", i, prediction);


    // safely add the adjustment to find the threshold
    uint8_t thresh;
    if((prediction + (int)adjustment)>250){
        thresh = 250;
    }else if((prediction + (int)adjustment)<5){
        thresh = 5;
    }else{
        thresh = prediction + adjustment;
    }

    return thresh;
}






uint8_t mcv_threshold_otsu_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

   uint8_t thresh = _calc_thresh_otsu_partial(im_w, im_h, x, y, w, h, im, adjustment);

    for (int ty = y; ty < y+h; ty++) {
        for (int tx = x; tx < x+w; tx++) {
            uint8_t v = im[(ty*im_w) +tx];
            if (v >= thresh){
                threshim[(ty*im_w) +tx] = 255;
            }else {
                threshim[(ty*im_w) +tx] = 0;
            }
        }
    }
    return thresh;
}


inline uint8_t
BilinearInterpolation(uint8_t q11, uint8_t q12, uint8_t q21, uint8_t q22, \
                        int x1, int x2, int y1, int y2, \
                        int x, int y)
{
    int x2x1 = x2 - x1;
    int y2y1 = y2 - y1;
    int x2x = x2 - x;
    int y2y = y2 - y;
    int yy1 = y - y1;
    int xx1 = x - x1;

    // return 1.0 / (x2x1 * y2y1) * (
    //     q11 * x2x * y2y +
    //     q21 * xx1 * y2y +
    //     q12 * x2x * yy1 +
    //     q22 * xx1 * yy1
    // );

        return (
        q11 * x2x * y2y +
        q21 * xx1 * y2y +
        q12 * x2x * yy1 +
        q22 * xx1 * yy1) / (x2x1 * y2y1);
}


uint8_t mcv_threshold_otsu_tile_partial(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    int i,j;

    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

    // figure out how many tiles we need to get roughly square tiles
    // long edge gets 3, short edge gets fewer
    int nx, ny;
    const int tmax = 3;
    if(w>=h){
        nx = tmax;
        float target = w/nx;
        ny = roundf(((float)h)/target);
    }else{
        ny = tmax;
        float target = h/ny;
        nx = roundf(((float)w)/target);
    }
    if(nx<1) nx = 1;
    if(ny<1) ny = 1;


    // tile edge lengths
    int tile_w = w/nx;
    int tile_h = h/ny;

    int thresh_sum = 0;

    for(i=0; i<ny; i++)
    {
        int y_this = y+(i*tile_h);

        for(j=0; j<nx; j++)
        {
            int x_this = x+(j*tile_w);
            uint8_t thresh = _calc_thresh_otsu_partial(im_w, im_h, x_this, y_this, tile_w, tile_h, im, adjustment);

            thresh_sum += thresh;

            // this is the non-interpolated method
            for (int ty = y_this; ty < y_this+tile_h; ty++) {
                for (int tx = x_this; tx < x_this+tile_w; tx++) {
                    uint8_t v = im[(ty*im_w) +tx];
                    if (v >= thresh){
                        threshim[(ty*im_w) +tx] = 255;
                    }else {
                        threshim[(ty*im_w) +tx] = 0;
                    }
                }
            }
        }
    }
    return thresh_sum / (nx*ny);
}


static inline void _constrain(int* i)
{
    if(*i>254) *i=254;
    if(*i<1)  *i=1;
}

uint8_t mcv_threshold_otsu_tile_partial_interpolated(const int im_w, const int im_h, const int x, const int y, const int w, const int h, const uint8_t *im, uint8_t *threshim, int adjustment)
{
    int i,j;

    // set up threshim with im data, since we are preparing to partially threshold
    memcpy( threshim, im, im_w * im_h);

    // figure out how many tiles we need to get roughly square tiles
    // long edge gets 3, short edge gets fewer
    int nx, ny;
    const int tmax = 3;
    if(w>=h){
        nx = tmax;
        float target = w/nx;
        ny = roundf(((float)h)/target);
    }else{
        ny = tmax;
        float target = h/ny;
        nx = roundf(((float)w)/target);
    }
    if(nx<1) nx = 1;
    if(ny<1) ny = 1;


    // tile edge lengths
    int tile_w = w/nx;
    int tile_h = h/ny;
    int tile_wh = tile_w * tile_h;

    // threshold values and xy centers of the tiles for later interpolation
    // include space for a border
    int q[ny+2][nx+2];
    int qx[nx+2];
    int qy[ny+2];

    int thresh_sum = 0;

    // first time through just find the course grid of otsu cutoff values
    for(i=0; i<ny; i++)
    {
        int y_this = y+(i*tile_h);
        qy[i+1] = y_this+(tile_h/2);

        for(j=0; j<nx; j++)
        {
            int x_this = x+(j*tile_w);
            q[i+1][j+1] = _calc_thresh_otsu_partial(im_w, im_h, x_this, y_this, tile_w, tile_h, im, adjustment);
            qx[j+1] = x_this+(tile_w/2);
            thresh_sum += q[i+1][j+1];
        }
    }

    // add xy coordinates for the border
    qx[0]    = qx[1] -tile_w;
    qx[nx+1] = qx[nx]+tile_w;
    qy[0]    = qy[1] -tile_h;
    qy[ny+1] = qy[ny]+tile_h;

    // extend the left/right edges out.
    if(nx==1){
        // only 1 tile wide, nothing to extrapolate so copy
        for(i=1;i<=ny;i++){
            q[i][0]=q[i][1];     // left edge
            q[i][nx+1]=q[i][nx]; // right edge
        }
    }else{
        // extrapolate
         for(i=1;i<=ny;i++){
            q[i][0] = (2*q[i][1]) - q[i][2];     // left edge
            q[i][nx+1]= (2*q[i][nx]) - q[i][nx-1]; // right edge
        }
    }


    // extend top/bottom edges out
    if(ny==1){
        // only 1 tile high, nothing to extrapolate so copy
        for(i=1;i<=nx;i++){
            q[0][i]=q[1][i];     // top edge
            q[ny+1][i]=q[ny][i]; // btm edge
        }
    }else{
        for(i=1;i<=nx;i++){
            q[0][i] = (2*q[1][i]) - q[2][i];     // top edge
            q[ny+1][i] = (2*q[ny][i]) - q[ny-1][i]; // btm edge
        }
    }

    // we just did some extrapolation, so bound the values sensibly
    for(i=1;i<=ny;i++){
        _constrain(&q[i][0]);     // top edge
        _constrain(&q[i][nx+1]); // btm edge
    }
    for(i=1;i<=nx;i++){
        _constrain(&q[0][i]);     // top edge
        _constrain(&q[ny+1][i]); // btm edge
    }

    // set corners as average of neighbors.
    // TODO could extrapolate here as well which might help in far corners
    // in cases with extreme lens shading
    q[0][0]       = (q[0][1]     + q[1][0])    / 2;
    q[0][nx+1]    = (q[0][nx]    + q[1][nx+1]) / 2;
    q[ny+1][0]    = (q[ny][0]    + q[ny+1][1]) / 2;
    q[ny+1][nx+1] = (q[ny+1][nx] + q[ny][nx+1]) / 2;

    // for now, just fudge the corners down to help lens shading
    q[0][0]       -= 12;
    q[0][nx+1]    -= 12;
    q[ny+1][0]    -= 12;
    q[ny+1][nx+1] -= 12;

    // constrain corners to be safe
    _constrain(&q[0][0]);
    _constrain(&q[0][nx+1]);
    _constrain(&q[ny+1][0]);
    _constrain(&q[ny+1][nx+1]);

    // // print our map for debug
    // printf("qx: ");
    // for(i=0;i<=nx+1;i++) printf(" %4d", qx[i]);
    // printf("\nqy: ");
    // for(j=0;j<=ny+1;j++) printf(" %4d", qy[j]);
    // printf("\nq:\n");
    // for(i=0;i<=ny+1;i++){
    //     for(j=0;j<=nx+1;j++){
    //         printf(" %3d", q[i][j]);
    //     }
    //     printf("\n");
    // }
    //  printf("\n");


    // do first pass, calculating gradient across x (left right) only for each row
    int pass1[ny+2][w];
    for(i=0;i<=ny+1;i++){
        int k=0;
        for(j=x;j<x+w;j++){
            pass1[i][j-x] = (q[i][k]*(qx[k+1]-j)) + (q[i][k+1]*(j-qx[k]));
            // if(i==1){
            //     printf("j: %4d pass1: %d\n", j, pass1[i][j-x]/tile_w);
            // }
            if(j>=qx[k+1]){
                //printf("k %d->%d at j=%d\n", k, k+1, j);
                k++;
            }
        }
    }

    // now interpolate and threshold
    // this is NOT OPTIMIZED YET
    //
    int l = 0; // y row index of course tile grid
    for(int i = y; i < y+h; i++){

        int tmp1 = (qy[l+1]-i);
        int tmp2 = (i-qy[l]);

        for(int j = x; j < x+w; j++){

            uint8_t thresh;
            thresh = ((pass1[l][j-x]*tmp1)+(pass1[l+1][j-x]*tmp2))/tile_wh;

            uint8_t v = im[(i*im_w) +j];
            if (v >= thresh){
                threshim[(i*im_w) +j] = 255;
            }else {
                threshim[(i*im_w) +j] = 0;
            }

            // uncomment for a smooth pretty picture of the threshold gradient
            //threshim[(i*im_w)+j] = thresh;
        }
        // bump to the next row of pass-1 data
        if(i>=qy[l+1]) l++;
    }

    // // highlight the centers of our interpolation points
    // for(i=1;i<=ny;i++){
    //     for(j=1;j<=nx;j++){
    //         threshim[(qy[i]*im_w)+qx[j]]=255;
    //     }
    // }

    return thresh_sum / (nx*ny);
}


// make a single pixel white border so that opencv can still detect black squares
// of the chessboard that go outside the perimeter, this lets us fill a larger
// percentage of the target squares and cover more of the lens FOV
static void _populate_white_border(const int im_w, \
                                   const int x, const int y, \
                                   const int w, const int h, \
                                   uint8_t *im)
{
    int c,r;

    // printf("imw: %3d  xy %3d %3d  wh %3d %3d\n", im_w, x, y, w, h);

    // top row
    int start = (im_w*y)+x;
    int end = start + w;
    for(c=start; c<end; c++) im[c] = 255;

    // bottom row
    start = (im_w*(y+h-1))+x;
    end = start + w;
    for(c=start; c<end; c++) im[c] = 255;

    // left/right
    for(r=y; r<(y+h); r++){
        im[(r*im_w)+x]       = 255;
        im[(r*im_w)+x+w-1]     = 255;
    }

    return;
}


uint8_t  mcv_threshold_partial(const int im_w, const int im_h, \
                            const int x, const int y, \
                            const int w, const int h, \
                            const uint8_t *im, uint8_t *threshim,\
                            int adjustment)
{

    int ret;
    // return mcv_threshold_mean_partial(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    // return mcv_threshold_tile_partial(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    // return mcv_threshold_median_partial(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    // return mcv_threshold_otsu_partial(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    // return mcv_threshold_otsu_tile_partial(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    ret = mcv_threshold_otsu_tile_partial_interpolated(im_w, im_h, x, y, w, h, im, threshim, adjustment);
    _populate_white_border(im_w, x, y, w, h, threshim);
    return ret;
}
