/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <limits>
#include <cstdio>
#include <queue>
#include <regex>
#include <string>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>

#include <voxl_cutils.h>

#include "calibrator.h"
#include "undistort.h"
#include "threshold.h"

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/highgui.hpp>

#define min(x,y) ((x) > (y) ? (y) : (x))

#define STEREO_RECT_THICKNESS 3

#define FULL_AREA (imageSize.width * imageSize.height)

// TODO add an override for these cutoffs
#define PLUMB_INTR_SUCCESS_CUTOFF       0.75
#define FISHEYE_INTR_SUCCESS_CUTOFF     0.60
#define EXTR_SUCCESS_CUTOFF             ((2*(double)(FULL_AREA))/614400)
#define SUBPIX_WINDOW 5
#define THERMAL_RES_CUTOFF 19200

#define OVERLAP_TIME_THRESH 2000000000


// tiny adjustment for the threshold above average to help with glare
// not needed anymore since we did interpolated thresholding
//#define ADJUSTMENT (-35)
#define ADJUSTMENT (0)

// extra large adjustment for the thermal cal card
// so it only picks up the hot dots and not user's hands
#define THERMAL_ADJUSTMENT 70

using namespace cv;
using namespace std;

extern calib_params cp;
extern char input_pipe[MODAL_PIPE_MAX_DIR_LEN];
extern bool extrinsics_only;
extern bool skip_extrinsics;
extern bool use_thresh_overlay;
extern bool mirror_image;
extern bool no_target_extrinsics;
extern bool thermal_cal;

static bool running_extrinsics;
static bool capture_frame = false;
static bool finish_sampling = false;

static int finished_calibrating = 0;
static int calibration_passed = 0;

pthread_cond_t consumer_cond;
pthread_mutex_t consumer_mutex;

//These two should only be referenced when in posession of the consumer mutex
char *consumer_frame;
camera_image_metadata_t consumer_meta;

// Variables for opencv cal functions
vector<board_params> bp_vec;
Size imageSize;

#define SCALED_COLOR(cur, target) (Scalar(255 - (255*((cur*cur*cur*cur*cur)/(target*target*target*target*target))), 255 * ((cur*cur*cur*cur*cur)/(target*target*target*target*target)), 0))

/////////////////////////////////////////////////////////////////////////////////////////
// Helper functions for calculating the parameters from a given chessboard detection
/////////////////////////////////////////////////////////////////////////////////////////

static float _corners_area(calib_params cp, vector<Point2f> corners){
    Point2f a, b, c, d;
    float e, f, g, h;
    a = corners[0];
    b = corners[cp.board_size.width -1];
    d = corners[cp.board_size.width * (cp.board_size.height - 1)];
    c = corners[(cp.board_size.width * cp.board_size.height) - 1];

    e = (a.x * b.y) - (a.y * b.x);
    f = (b.x * c.y) - (b.y * c.x);
    g = (c.x * d.y) - (c.y * d.x);
    h = (d.x * a.y) - (d.y * a.x);

    return (abs(e + f + g + h)/2);
}

static bool _valid_points(vector<Point2f> pointBuf, int x1, int x2, int y1, int y2){

    for(Point2f pt : pointBuf){
        if(pt.x < x1 || pt.x > x2 || pt.y < y1 || pt.y > y2){
            return false;
        }
    }
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Creates a padded overlay with total collected progress bar
/////////////////////////////////////////////////////////////////////////////////////////
static Mat _make_padded_overlay_mono(Mat rgb, float pct){
    char buf[64];
    Mat padded_overlay;

    if (rgb.cols < 640 && rgb.rows < 480){
        cv::resize(rgb, rgb, cv::Size(), 480/rgb.rows, 640/rgb.cols);
    }

    const float size = rgb.rows/12;

    copyMakeBorder(rgb, padded_overlay, size, size/2, 0, 0, BORDER_CONSTANT);

    rectangle(padded_overlay, Point(0, 0), Point(pct*padded_overlay.cols, size/2), GREEN, -1); //detections saved
    sprintf(buf, "Total Collected: %3.0f%%", pct*100);
    putText(padded_overlay, buf, Point(0.36*padded_overlay.cols, size*.875), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);

    sprintf(buf, "Expected Board: %dx%d %6.4fm", cp.board_size.width, cp.board_size.height, cp.square_size);
    putText(padded_overlay, buf, Point(0.31*padded_overlay.cols, padded_overlay.rows - (size * 0.15)), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);
    return padded_overlay;
}

static Mat _make_padded_overlay_stereo(Mat rgb_l, Mat rgb_r, float pct){
    char buf[64];
    Mat padded_overlay;

    //Arbitrary size 1/12th of image to make the border at the top
    const float size = rgb_l.rows/12;

    Mat l_down, r_down, merged;

    pyrDown(rgb_l, l_down);
    pyrDown(rgb_r, r_down);

    hconcat(l_down, r_down, merged);

    copyMakeBorder(merged, padded_overlay, size, size/2, 0, 0, BORDER_CONSTANT);

    if(!running_extrinsics){
        rectangle(padded_overlay, Point(0, 0), Point(pct*padded_overlay.cols, size/2), GREEN, -1); //detections saved
        sprintf(buf, "Total Collected: %3.0f%%", pct*100);
    } else {
        sprintf(buf, "Total Collected: %d", (int)pct);
        pct = pct * 5.0 / 100;
        rectangle(padded_overlay, Point(0, 0), Point(pct*padded_overlay.cols, size/2), GREEN, -1); //detections saved
    }
    putText(padded_overlay, buf, Point(0.36*padded_overlay.cols, size*.875), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);

    putText(padded_overlay, "Left", Point(0.225*padded_overlay.cols, size*.875), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);
    putText(padded_overlay, "Right", Point(0.725*padded_overlay.cols, size*.875), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);

    sprintf(buf, "Expected Board: %dx%d %6.4fm", cp.board_size.width, cp.board_size.height, cp.square_size);
    putText(padded_overlay, buf, Point(0.31*padded_overlay.cols, padded_overlay.rows - (size * 0.15)), FONT_HERSHEY_SIMPLEX, size/80, WHITE, size/40, LINE_AA);

    return padded_overlay;
}

static Mat getRedImage(int w, int h){

    Mat img(h, w, CV_8UC3);
    rectangle(img, Point(0,0), Point(w,h), Scalar(255,0,0), -1);

    return img;
}

static void addRedTint(Mat mat){

    Mat red_img = getRedImage(mat.cols, mat.rows);

    addWeighted(mat, 0.75, red_img, 0.25, 0, mat);
}

/////////////////////////////////////////////////////////////////////////////////////////
// helper functions for error calculation
/////////////////////////////////////////////////////////////////////////////////////////

static void calc_board_corner_positions(Size board_size, float square_size, vector<Point3f>& corners){
    corners.clear();
    for( int i = 0; i < board_size.height; ++i )
        for( int j = 0; j < board_size.width; ++j )
            corners.push_back(Point3f(j*square_size, i*square_size, 0));
}

bool validate_params(calib_params cp){
    if (cp.board_size.width <= 0 || cp.board_size.height <= 0){
        cerr << "Invalid Board size: " << cp.board_size.width << " " << cp.board_size.height << endl;
        return false;
    }
    if ((double)cp.square_size <= 10e-6){
        cerr << "Invalid square size " << cp.square_size << endl;
        return false;
    }

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Various sets of potential sets of target areas to get frames from
/////////////////////////////////////////////////////////////////////////////////////////
#define TARGET_SET_FISHEYE  initializeTargetsFisheye()
#define TARGET_SET_FISHEYE_WIDE  initializeTargetsFisheyeWide()
#define TARGET_SET_STANDARD initializeTargetsStandard()
#define TARGET_SET_EXTRINSICS initializeTargetsExtrinsics()
#define TARGET_SET_THERMAL initializeTargetsThermal()


//2x2 grid
static queue<vector<int>> initializeTargetsFisheye(){

    queue<vector<int>> q;

    //Each item represents a desired square in percentages of the image {x1,y1,x2,y2, fill threshold, #samples} from top left
    q.push({00, 00, 70, 60, 45, 3});
    q.push({30, 00,100, 60, 45, 3});
    q.push({30, 40,100,100, 45, 3});
    q.push({00, 40, 70,100, 45, 3});
    q.push({15, 10, 85, 90, 50, 3}); // center

    return q;
}

static queue<vector<int>> initializeTargetsFisheyeWide(){

    queue<vector<int>> q;

    //Each item represents a desired square in percentages of the image {x1,y1,x2,y2, fill threshold, #samples} from top left
    // q.push({25, 00, 75, 60, 55, 3}); // top
    // q.push({45, 20, 95, 80, 55, 3}); // right
    // q.push({25, 40, 75,100, 55, 3}); // bottom
    // q.push({05, 20, 55, 80, 55, 3}); // left
    // q.push({20, 15, 80, 85, 55, 3}); // center

    q.push({00, 00, 50, 60, 40, 3});
    q.push({50, 00,100, 60, 40, 3});
    q.push({50, 40,100,100, 40, 3});
    q.push({00, 40, 50,100, 40, 3});
    q.push({15, 10, 85, 90, 45, 3}); // center

    return q;
}

static queue<vector<int>> initializeTargetsStandard(){

    queue<vector<int>> q;

    //Each item represents a desired square in percentages of the image {x1,y1,x2,y2, fill threshold, #samples} from top left
    //2x2 grid filling the whole frame
    q.push({00, 00, 60, 60, 45, 2});
    q.push({40, 00,100, 60, 45, 2});
    q.push({40, 40,100,100, 45, 2});
    q.push({00, 40, 60,100, 45, 2});
    q.push({15, 10, 85, 90, 45, 3}); // center

    // Horizontal Skew I struggled with reprojection error using these ones, disabled for now
    // q.push({15, 15, 45, 85, 30, 2});
    // q.push({65, 15, 95, 85, 30, 2});

    // Vertical Skew
    q.push({25, 10, 75, 45, 30, 2});
    q.push({25, 55, 75, 90, 30, 2});

    // //Far boxes
    // q.push({10, 10, 35, 40, 50, 2});
    // q.push({65, 60, 90, 90, 50, 2});

    // big skew
    // q.push({15,  5, 85, 55, 40, 2});
    // q.push({15, 45, 85, 95, 40, 2});

    // sides
    q.push({0,  0, 50,  100, 45, 2});
    q.push({25, 0, 75,  100, 45, 2});
    q.push({50, 0, 100, 100, 45, 2});


    return q;
}

static queue<vector<int>> initializeTargetsThermal(){

    queue<vector<int>> q;

    //Each item represents a desired square in percentages of the image {x1,y1,x2,y2, fill threshold, #samples} from top left
    //2x2 grid filling the whole frame
    q.push({00, 00, 60, 60, 50, 2});
    q.push({40, 00,100, 60, 50, 2});
    q.push({40, 40,100,100, 50, 2});
    q.push({00, 40, 60,100, 50, 2});

    //Horizontal Skew
    q.push({15, 15, 45, 85, 35, 2});
    q.push({65, 15, 95, 85, 35, 2});

    return q;
}

static queue<vector<int>> initializeTargetsExtrinsics(){

    queue<vector<int>> q;

    //Each item represents a desired square in percentages of the image {x1,y1,x2,y2, fill threshold, camera_id (0 or 1, L and R accordingly)} from top left
    //last index (6) is boolean for should the non-target caemra reuse the threshold cutoff T calculated for the target camera
    // Close boxes
    q.push({40, 00, 100, 100, 40, 0, 0});
    q.push({00, 00, 60,  100, 40, 1, 0});


    // Far boxes
    q.push({10, 10, 35, 40, 40, 0, 1}); // top left
    q.push({65, 10, 90, 40, 40, 0, 1}); // top right
    q.push({65, 60, 90, 90, 40, 0, 1}); // bottom right
    q.push({10, 60, 35, 90, 40, 0, 1}); // bottom left
    q.push({38, 36, 62, 64, 40, 0, 1}); // center

    return q;
}

static queue<vector<int>> getTargets(int w, int h)
{

    double ratio = (double)w/(double)h;

    if(cp.use_fisheye){
        if(ratio<1.45){
            return TARGET_SET_FISHEYE;
        }else{
            return TARGET_SET_FISHEYE_WIDE;
        }
    } else if (thermal_cal){
        return TARGET_SET_THERMAL;
    } else if (extrinsics_only) {
        return TARGET_SET_EXTRINSICS;
    } else {
        return TARGET_SET_STANDARD;
    }
}

static cv::Ptr<cv::SimpleBlobDetector> setup_circle_detector(int width, __attribute__((unused))int height)
{
    cv::SimpleBlobDetector::Params params;

    // TODO maybe tweak these for different cameras
    params.filterByArea = true;
    params.minArea = 1;
    params.blobColor = 255;
    params.filterByColor = true;
    params.filterByCircularity = false;
    params.minCircularity = 0.8;
    params.maxCircularity = std::numeric_limits<float>::max();
    params.filterByInertia = true;
    params.minInertiaRatio = 0.1;
    params.minDistBetweenBlobs = 0.5;

    // smaller blobs for lepton
    if(width<300){
        params.maxArea = 120;
    }
    else{
        params.maxArea = 300;
    }
  
    cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    return detector;
}

void set_result(int passed)
{
    finished_calibrating = 1;
    calibration_passed = passed;
    return;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Handle the recieving of mono/stereo frames and pulling chessboards out of them
/////////////////////////////////////////////////////////////////////////////////////////
static bool process_mono_frame(calib_params cp, camera_image_metadata_t meta, char* frame, vector<vector<Point2f>>& imagePoints)
{

    //Setup static variables
    //static int chessBoardFlags = CALIB_CB_NORMALIZE_IMAGE | (cp.use_fisheye ? 0 : CALIB_CB_FAST_CHECK);
    //static int chessBoardFlags = (cp.use_fisheye ? 0 : CALIB_CB_FAST_CHECK);

    // for chessboard don't use the normalization or fast check flags anymore.
    // we do our own thresholding that's much faster and more reliable than the normalization
    // and the fast check fails to detect the chessboard when it covers a large percentage
    // of the box area, particularly with fisheye cameras.
    static int chessBoardFlags = 0;

    static int circleGridFlags = CALIB_CB_SYMMETRIC_GRID | CALIB_CB_CLUSTERING;

    static int w_raw = meta.width;
    static int h_raw = meta.height;
    //static bool resize = (w_raw != 640 || h_raw != 480);
    //static bool resize = false;

    //List of target sections that we want frames of
    static queue<vector<int>> targets = getTargets(w_raw, h_raw);

    static int maxTargets = targets.size();

    static vector<int> target = targets.front();

    static int target_x1 = target[0] * w_raw / 100;
    static int target_y1 = target[1] * h_raw / 100;
    static int target_x2 = (target[2] * w_raw / 100);
    static int target_y2 = (target[3] * h_raw / 100);
    static int targetThreshold = target[4];
    static int frameCount = target[5];
    static int targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
    static Rect targetRect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);
    static Mat mono_thresh(h_raw, w_raw, CV_8UC1);

    //Convert to Mat
    Mat mono_raw(h_raw, w_raw, CV_8UC1, frame);

    // Apply Thresholding
    int8_t adjustment = ADJUSTMENT;
    if(thermal_cal) adjustment = THERMAL_ADJUSTMENT;
    mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), mono_raw.data, mono_thresh.data, adjustment);

    Mat mono_color, mono_overlay = use_thresh_overlay ? mono_thresh : mono_raw;
    Mat in[] = {mono_overlay, mono_overlay, mono_overlay};
    merge(in, 3, mono_color);

    vector<Point2f> pointBuf;

    static cv::Ptr<cv::SimpleBlobDetector> detector = setup_circle_detector(w_raw, h_raw);

    bool success;
    if(thermal_cal){
        success = findCirclesGrid(mono_raw, cp.board_size, pointBuf, circleGridFlags, detector);
    }else{
        success = findChessboardCorners(mono_thresh, cp.board_size, pointBuf, chessBoardFlags);
    }

    if (success){
        //Run subpixel accuracy on the original image since we found the corners on the thresholded image
        // this only works when we detect a corner in the first place, not a blob from the circle detector
        if (!thermal_cal) cornerSubPix(mono_raw, pointBuf, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1), TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
        drawChessboardCorners(mono_color, cp.board_size, Mat(pointBuf), true);

        if(_valid_points(pointBuf, target_x1, target_x2, target_y1, target_y2)){

            float sampleArea = _corners_area(cp, pointBuf)/targetArea;

            rectangle(mono_color, targetRect, SCALED_COLOR(sampleArea*100, targetThreshold));

            if (sampleArea * 100 >= targetThreshold){
                imagePoints.push_back(pointBuf);

                if(!--frameCount){
                    targets.pop();

                    if(targets.size()){

                        target = targets.front();

                        target_x1 = target[0] * w_raw / 100;
                        target_y1 = target[1] * h_raw / 100;
                        target_x2 = (target[2] * w_raw / 100);
                        target_y2 = (target[3] * h_raw / 100);
                        targetThreshold = target[4];
                        frameCount = target[5];
                        targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
                        targetRect = Rect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);
                    }
                }

            }
        } else {
            //Target area for the chessboard
            rectangle(mono_color, targetRect, RED);
        }

    } else {
        //Target area for the chessboard
        rectangle(mono_color, targetRect, RED);
    }

    if(mirror_image){
        flip(mono_color, mono_color, 1);
    }

    Mat padded = _make_padded_overlay_mono(mono_color, 1 - (((float)targets.size())/maxTargets));
    camera_image_metadata_t overlay_meta = meta;
    overlay_meta.format = IMAGE_FORMAT_RGB;
    overlay_meta.width  = padded.cols;
    overlay_meta.stride = padded.cols * 3;
    overlay_meta.height = padded.rows;
    overlay_meta.size_bytes = padded.rows * padded.cols * 3;
    pipe_server_write_camera_frame(OVERLAY_CH, overlay_meta, (char*)padded.data);

    //Return true if we have all of our samples
    return targets.size() == 0;
}

static bool process_stereo_frame(calib_params cp,
                          camera_image_metadata_t meta,
                          char* frame,
                          vector<vector<Point2f>>* imagePoints)
{

    vector<vector<Point2f>>& imagePoints_l = imagePoints[0];
    vector<vector<Point2f>>& imagePoints_r = imagePoints[1];
    vector<vector<Point2f>>& imagePointsOverlap_l = imagePoints[2];
    vector<vector<Point2f>>& imagePointsOverlap_r = imagePoints[3];


    //Static for stereo processing
    constexpr int STATUS_LEFT    = 0;
    constexpr int STATUS_RIGHT   = 1;
    constexpr int STATUS_OVERLAP = 2;
    static int status = (extrinsics_only ? STATUS_OVERLAP : STATUS_LEFT);

    //Setup static variables
    static int chessBoardFlags = CALIB_CB_NORMALIZE_IMAGE;

    static int w_raw = meta.width;
    static int h_raw = meta.height;
    static float pctDone = 0.0;

    //List of target sections that we want frames of
    static queue<vector<int>> targets = getTargets(w_raw, h_raw);
    static int maxTargets = targets.size();
    static vector<int> target = targets.front();

    static int target_x1 = target[0] * w_raw / 100;
    static int target_y1 = target[1] * h_raw / 100;
    static int target_x2 = (target[2] * w_raw / 100);
    static int target_y2 = (target[3] * h_raw / 100);
    static int targetThreshold = target[4];
    static int frameCount = target[5];
    static int targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
    static Rect targetRect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);
    static Mat left_thresh(h_raw, w_raw, CV_8UC1), right_thresh(h_raw, w_raw, CV_8UC1);
    static Mat left_color(h_raw, w_raw, CV_8UC3);
    static Mat right_color(h_raw, w_raw, CV_8UC3);
    static Mat left_overlay;
    static Mat right_overlay;
    static vector<Point2f> pointBuf_l, pointBuf_r;

    bool done = false;

    //Convert to Mat
    Mat left_raw(h_raw, w_raw, CV_8UC1, frame), right_raw(h_raw, w_raw, CV_8UC1, &frame[w_raw*h_raw]);

    Mat padded;

    switch (status){

        case STATUS_LEFT:{
            //Apply Thresholding
            mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), left_raw.data, left_thresh.data, ADJUSTMENT);
            left_overlay = use_thresh_overlay ? left_thresh : left_raw;
            cv::Mat in_left[3] = {left_overlay, left_overlay, left_overlay};
            cv::merge(in_left, 3, left_color);

            //Deal with the other image
            right_overlay = right_raw;
            cv::Mat in_right[3] = {right_overlay, right_overlay, right_overlay};
            cv::merge(in_right, 3, right_color);
            addRedTint(right_color);

            //40% left targets, 40% right targets, 20% overlapping targets
            pctDone = 1.0-(((float)targets.size()) / maxTargets);

            if(findChessboardCorners(left_thresh, cp.board_size, pointBuf_l, chessBoardFlags)){

                //Run subpixel accuracy on the original image since we found the corners on the thresholded image
                cornerSubPix(left_raw, pointBuf_l, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                                TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));

                drawChessboardCorners(left_color, cp.board_size, Mat(pointBuf_l), true);

                if(_valid_points(pointBuf_l, target_x1, target_x2, target_y1, target_y2)){

                    float sampleArea = _corners_area(cp, pointBuf_l)/targetArea;

                    rectangle(left_color, targetRect, SCALED_COLOR(sampleArea*100, targetThreshold), STEREO_RECT_THICKNESS);

                    if(sampleArea * 100 >= targetThreshold){
                        imagePoints_l.push_back(pointBuf_l);

                        if(!--frameCount){
                            targets.pop();

                            if(!targets.size()){//No more targets for left, start right
                                status = STATUS_RIGHT;
                                targets = getTargets(w_raw, h_raw);
                            }

                            target = targets.front();

                            target_x1 = target[0] * w_raw / 100;
                            target_y1 = target[1] * h_raw / 100;
                            target_x2 = (target[2] * w_raw / 100);
                            target_y2 = (target[3] * h_raw / 100);
                            targetThreshold = target[4];
                            frameCount = target[5];
                            targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
                            targetRect = Rect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);
                        }
                    }
                } else {
                    rectangle(left_color, targetRect, RED, STEREO_RECT_THICKNESS);
                }

            } else {
                rectangle(left_color, targetRect, RED, STEREO_RECT_THICKNESS);
            }
            break;
        }

        case STATUS_RIGHT:{
            //Apply Thresholding
            mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), right_raw.data, right_thresh.data, ADJUSTMENT);
            right_overlay = use_thresh_overlay ? right_thresh : right_raw;
            cv::Mat in_right[3] = {right_overlay, right_overlay, right_overlay};
            cv::merge(in_right, 3, right_color);

            //Deal with the other image
            left_overlay = left_raw;
            cv::Mat in_left[3] = {left_overlay, left_overlay, left_overlay};
            cv::merge(in_left, 3, left_color);
            addRedTint(left_color);

            //40% left targets, 40% right targets, 20% overlapping targets
            pctDone = 1.0-(((float)targets.size()) / maxTargets);

            if(findChessboardCorners(right_thresh, cp.board_size, pointBuf_r, chessBoardFlags)){

                //Run subpixel accuracy on the original image since we found the corners on the thresholded image
                cornerSubPix(right_raw, pointBuf_r, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                                TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
                drawChessboardCorners(right_color, cp.board_size, Mat(pointBuf_r), true);

                if(_valid_points(pointBuf_r, target_x1, target_x2, target_y1, target_y2)){

                    float sampleArea = _corners_area(cp, pointBuf_r)/targetArea;

                    rectangle(right_color, targetRect, SCALED_COLOR(sampleArea*100, targetThreshold), STEREO_RECT_THICKNESS);

                    if(sampleArea * 100 >= targetThreshold){
                        imagePoints_r.push_back(pointBuf_r);

                        if(!--frameCount){
                            targets.pop();

                            if(!targets.size()){//No more targets for left, start right
                                if(skip_extrinsics){
                                    done = true;
                                    break;
                                }
                                else{
                                    status = STATUS_OVERLAP;
                                    targets = initializeTargetsExtrinsics();
                                }
                                break;
                            }

                            target = targets.front();

                            target_x1 = target[0] * w_raw / 100;
                            target_y1 = target[1] * h_raw / 100;
                            target_x2 = (target[2] * w_raw / 100);
                            target_y2 = (target[3] * h_raw / 100);
                            targetThreshold = target[4];
                            frameCount = target[5];
                            targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
                            targetRect = Rect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);
                        }
                    }
                } else {
                    rectangle(right_color, targetRect, RED, STEREO_RECT_THICKNESS);
                }

            } else {
                rectangle(right_color, targetRect, RED, STEREO_RECT_THICKNESS);
            }
            break;
        }

        case STATUS_OVERLAP:
            // this is extrinsics mode with no targets, (when using enter key to grab frame)
            if (no_target_extrinsics){
                // Apply Thresholding, use the same cutoff for both to save compute
                uint8_t t = mcv_threshold_partial(w_raw, h_raw, 0, 0, w_raw, h_raw, left_raw.data, left_thresh.data, ADJUSTMENT);
                mcv_threshold_fixed(w_raw, h_raw, right_raw.data, right_thresh.data, t);

                left_overlay = use_thresh_overlay ? left_thresh : left_raw;
                right_overlay = use_thresh_overlay ? right_thresh : right_raw;

                cv::Mat in_left[3] = {left_overlay, left_overlay, left_overlay};
                cv::merge(in_left, 3, left_color);
                cv::Mat in_right[3] = {right_overlay, right_overlay, right_overlay};
                cv::merge(in_right, 3, right_color);

                {
                    static int dummy __attribute__((unused)) =
                        printf("Sampling overlapping images now\nEnter an empty line to take a sample or \"stop\" to finish sampling\n");
                }

                //User determines which frames to use
                running_extrinsics = true;
                pctDone = imagePointsOverlap_l.size();
                bool found = findChessboardCorners(left_thresh,  cp.board_size, pointBuf_l, chessBoardFlags)
                    && findChessboardCorners(right_thresh, cp.board_size, pointBuf_r, chessBoardFlags);

                done = finish_sampling;

                if(found){
                    cornerSubPix(left_raw, pointBuf_l, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                                TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
                    cornerSubPix(right_raw, pointBuf_r, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                                TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
                    drawChessboardCorners(left_color, cp.board_size, Mat(pointBuf_l), true);
                    drawChessboardCorners(right_color, cp.board_size, Mat(pointBuf_r), true);

                    if (capture_frame){
                        imagePointsOverlap_l.push_back(pointBuf_l);
                        imagePointsOverlap_r.push_back(pointBuf_r);
                        capture_frame = false;
                    }
                }
                break;
            }
            // this is extrinsics mode with targets
            else {
                if (targets.empty()){
                    done = true;
                    break;
                }
                target = targets.front();

                target_x1 = target[0] * w_raw / 100;
                target_y1 = target[1] * h_raw / 100;
                target_x2 = (target[2] * w_raw / 100);
                target_y2 = (target[3] * h_raw / 100);
                targetThreshold = target[4];

                uint8_t targetCam = target[5];
                targetArea = abs((target_x1 - target_x2) * (target_y1 - target_y2));
                targetRect = Rect(target_x1, target_y1, target_x2 - target_x1, target_y2 - target_y1);

                // if (!targetCam){
                //     mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), left_raw.data, left_thresh.data, ADJUSTMENT);
                //     mcv_threshold_partial(w_raw, h_raw, 0, 0, w_raw, h_raw, right_raw.data, right_thresh.data, ADJUSTMENT);
                // }
                // else{
                //     mcv_threshold_partial(w_raw, h_raw, 0, 0, w_raw, h_raw, left_raw.data, left_thresh.data, ADJUSTMENT);
                //     mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), right_raw.data, right_thresh.data, ADJUSTMENT);
                // }


                uint8_t t;

                // if(!targetcam) means the left image has the target box
                if(!targetCam){
                    t = mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), left_raw.data, left_thresh.data, ADJUSTMENT);
                    if(target[6]){ // only on far boxes, reuse threshold to save compute
                        mcv_threshold_fixed(w_raw, h_raw, right_raw.data, right_thresh.data, t);
                    }else{
                        mcv_threshold_partial(w_raw, h_raw, 0, 0, w_raw, h_raw, right_raw.data, right_thresh.data, ADJUSTMENT);
                    }
                }
                else{
                    t = mcv_threshold_partial(w_raw, h_raw, target_x1, target_y1, (target_x2- target_x1), (target_y2 - target_y1), right_raw.data, right_thresh.data, ADJUSTMENT);
                    if(target[6]){ // only on far boxes, reuse threshold to save compute
                        mcv_threshold_fixed(w_raw, h_raw, left_raw.data, left_thresh.data, t);
                    }else{
                        mcv_threshold_partial(w_raw, h_raw, 0, 0, w_raw, h_raw, left_raw.data, left_thresh.data, ADJUSTMENT);
                    }
                }



                left_overlay = use_thresh_overlay ? left_thresh : left_raw;
                right_overlay = use_thresh_overlay ? right_thresh : right_raw;

                cv::Mat in_left[3] = {left_overlay, left_overlay, left_overlay};
                cv::merge(in_left, 3, left_color);
                cv::Mat in_right[3] = {right_overlay, right_overlay, right_overlay};
                cv::merge(in_right, 3, right_color);

                bool found = findChessboardCorners(left_thresh,  cp.board_size, pointBuf_l, chessBoardFlags)
                    && findChessboardCorners(right_thresh, cp.board_size, pointBuf_r, chessBoardFlags);

                if (found){
                    cornerSubPix(left_raw, pointBuf_l, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                            TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
                    cornerSubPix(right_raw, pointBuf_r, Size(SUBPIX_WINDOW, SUBPIX_WINDOW), Size(-1,-1),
                            TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1));
                    drawChessboardCorners(left_color, cp.board_size, Mat(pointBuf_l), true);
                    drawChessboardCorners(right_color, cp.board_size, Mat(pointBuf_r), true);

                    bool valid_pts;
                    if (!targetCam) valid_pts = _valid_points(pointBuf_l, target_x1, target_x2, target_y1, target_y2);
                    else valid_pts = _valid_points(pointBuf_r, target_x1, target_x2, target_y1, target_y2);

                    if(valid_pts){
                        float sampleArea = _corners_area(cp, pointBuf_l)/targetArea;

                        if(sampleArea * 100 >= targetThreshold){

                            imagePointsOverlap_l.push_back(pointBuf_l);
                            imagePointsOverlap_r.push_back(pointBuf_r);
                            targets.pop();
                        }

                        if (!targetCam) rectangle(left_color, targetRect, SCALED_COLOR(sampleArea*100, targetThreshold), STEREO_RECT_THICKNESS);
                        else rectangle(right_color, targetRect, SCALED_COLOR(sampleArea*100, targetThreshold), STEREO_RECT_THICKNESS);
                    }
                    else {
                        if (!targetCam) rectangle(left_color, targetRect, RED, STEREO_RECT_THICKNESS);
                        else rectangle(right_color, targetRect, RED, STEREO_RECT_THICKNESS);
                    }
                }
                else {
                    if (!targetCam) rectangle(left_color, targetRect, RED, STEREO_RECT_THICKNESS);
                    else rectangle(right_color, targetRect, RED, STEREO_RECT_THICKNESS);            
                }
                break;
            }
    }

    if(mirror_image){
        flip(left_color, left_color, 1);
        flip(right_color, right_color, 1);
    }

    padded = _make_padded_overlay_stereo(left_color, right_color, pctDone);
    camera_image_metadata_t overlay_meta = meta;
    overlay_meta.format = IMAGE_FORMAT_RGB;
    overlay_meta.width  = padded.cols;
    overlay_meta.stride = padded.cols * 3;
    overlay_meta.height = padded.rows;
    overlay_meta.size_bytes = padded.rows * padded.cols * 3;
    pipe_server_write_camera_frame(OVERLAY_CH, overlay_meta, (char*)padded.data);

    //Return true if we have all of our samples
    return done;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Actual mono and stereo calibration implementations
/////////////////////////////////////////////////////////////////////////////////////////
static bool run_calibration_mono(calib_params cp, vector<Mat>& rvecs, vector<Mat>& tvecs,
                                 float grid_width, vector<vector<Point2f>> imagePoints,
                                 Mat &cameraMatrix, Mat &distCoeffs, double &reprojErr)
{
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if (cp.use_fisheye){
        distCoeffs = Mat::zeros(4, 1, CV_64F);
    }
    else {
        distCoeffs = Mat::zeros(8, 1, CV_64F);
    }

    vector<vector<Point3f> > objectPoints(1);
    calc_board_corner_positions(cp.board_size, cp.square_size, objectPoints[0]);
    objectPoints[0][cp.board_size.width - 1].x = objectPoints[0][0].x + grid_width;
    objectPoints.resize(imagePoints.size(),objectPoints[0]);
    double max_error = PLUMB_INTR_SUCCESS_CUTOFF;

    if (cp.use_fisheye) {

        // const long flags =
        //         fisheye::CALIB_USE_INTRINSIC_GUESS |
        //         fisheye::CALIB_RECOMPUTE_EXTRINSIC |
        //         fisheye::CALIB_CHECK_COND |
        //         fisheye::CALIB_FIX_SKEW |
        //         fisheye::CALIB_FIX_K4 |
        //         fisheye::CALIB_FIX_K3;

        const long flags =
                fisheye::CALIB_USE_INTRINSIC_GUESS |
                fisheye::CALIB_RECOMPUTE_EXTRINSIC |
                fisheye::CALIB_CHECK_COND |
                fisheye::CALIB_FIX_SKEW;

        //const long flags = 0;


        // M0014 OV7252 fisheye tracking camera has focal length of about 278
        // and a width of 640. Ratio is about 0.43
        // M0149-1 AR0144 fisheye tracking has a focal length of about 554
        // and a width of 1280. Ratio is also about 0.43
        cameraMatrix.at<double>(0,0) = 0.43 * imageSize.width;
        cameraMatrix.at<double>(1,1) = 0.43 * imageSize.width;
        cameraMatrix.at<double>(0,2) = imageSize.width / 2;
        cameraMatrix.at<double>(1,2) = imageSize.height / 2;
        cameraMatrix.at<double>(2,2) = 1.0;
        max_error = FISHEYE_INTR_SUCCESS_CUTOFF;

        reprojErr = fisheye::calibrate(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs,
                                 tvecs, flags);

    }
    else {

        long flags = 0;

        // case for ov9782 cam on fpvRevB
        if(imageSize.width==1280 && imageSize.height==800){

            printf("using intrinsics guess for ov9782 (M0073) cam\n");
            flags = CALIB_USE_INTRINSIC_GUESS;
            cameraMatrix.at<double>(0,0) = 550;
            cameraMatrix.at<double>(1,1) = 550;
            cameraMatrix.at<double>(0,2) = imageSize.width / 2;
            cameraMatrix.at<double>(1,2) = imageSize.height / 2;
            cameraMatrix.at<double>(2,2) = 1.0;
        }
        // case for ov7251 stereo cams on Sentinel and Seeker
        else if(imageSize.width==640 && imageSize.height==480){
            printf("using intrinsics guess for ov7251 stereo cam\n");
            flags = CALIB_USE_INTRINSIC_GUESS;
            cameraMatrix.at<double>(0,0) = 495;
            cameraMatrix.at<double>(1,1) = 495;
            cameraMatrix.at<double>(0,2) = imageSize.width / 2;
            cameraMatrix.at<double>(1,2) = imageSize.height / 2;
            cameraMatrix.at<double>(2,2) = 1.0;
        }
        else{
            printf("unknown camera, not using intrinsics guess\n");
            flags = 0;
        }

        reprojErr = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
                              flags);
    }

    cout << "Matrix" << endl << cameraMatrix << endl;
    cout << "Distortion" << endl << distCoeffs << endl;
    cout << "distortion_model: " << (cp.use_fisheye ? "fisheye" : "plumb_bob") << endl;
    cout << "Re-projection error reported by calibrateCamera: "<< reprojErr << endl;

    if(reprojErr <= max_error){
        printf("Calibration " COLOR_GRN "Succeded!\n" RESET_FONT);
        return true;
    } else {
        printf("Calibration " COLOR_RED "Failed\n" RESET_FONT);
        printf("Max reprojection error: %4.2lf\n", max_error);
        printf("This is most likely due to motion blur, please try again.\n");
        return false;
    }
}

static bool intr_good;
static bool extr_good;
static bool is_vertical = false;

static int run_calibration_stereo(calib_params cp, vector<Mat>& rvecs_l, vector<Mat>& rvecs_r,
                                  vector<Mat>& tvecs_l, vector<Mat>& tvecs_r,
                                  Mat& cameraMatrix_l, Mat& cameraMatrix_r,
                                  Mat& distCoeffs_l, Mat& distCoeffs_r,
                                  float grid_width, Mat& R, Mat& T, vector<vector<Point2f>>* imagePoints,
                                  double &reprojErr_l, double &reprojErr_r, double &reprojErr_ext){


    vector<vector<Point2f>> imagePoints_l = imagePoints[0];
    vector<vector<Point2f>> imagePoints_r = imagePoints[1];
    vector<vector<Point2f>> imagePointsOverlap_l = imagePoints[2];
    vector<vector<Point2f>> imagePointsOverlap_r = imagePoints[3];


    if(!extrinsics_only){
        bool left, right;
        //Run left camera intrinsics
        printf(COLOR_LIT_BLU "Calibrating Left Camera\n" RESET_FONT);

        left = run_calibration_mono(cp, rvecs_l, tvecs_l, grid_width, imagePoints_l, cameraMatrix_l, distCoeffs_l, reprojErr_l);

        //Run right camera intrinsics
        printf(COLOR_LIT_BLU "Calibrating Right Camera\n" RESET_FONT);

        right = run_calibration_mono(cp, rvecs_r, tvecs_r, grid_width, imagePoints_r, cameraMatrix_r, distCoeffs_r, reprojErr_r);

        intr_good = left && right;

    } else {

        char fileName[128];

        // Convert input_pipe to std::string for processing
        string input_pipe_str(input_pipe);
        // Strip tags from input_pipe
        string base_input_name = strip_tags(input_pipe_str);

        // Convert the stripped base name back to a char array
        char base_input_name_c[MODAL_PIPE_MAX_DIR_LEN];
        strncpy(base_input_name_c, base_input_name.c_str(), MODAL_PIPE_MAX_DIR_LEN - 1);
        base_input_name_c[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0'; // Ensure null-termination

        // Use the stripped name for the file
        sprintf(fileName, "%sopencv_%s_intrinsics.yml", DATA_ROOT, base_input_name_c);
        printf("Pulling existing intrinsics from: %s\n", fileName);
        FileStorage intr(fileName, FileStorage::READ);

        intr["M1"]                  >> cameraMatrix_l;
        intr["M2"]                  >> cameraMatrix_r;
        intr["D1"]                  >> distCoeffs_l;
        intr["D2"]                  >> distCoeffs_r;
        intr["reprojection_error1"] >> reprojErr_l;
        intr["reprojection_error2"] >> reprojErr_r;

        printf("\n");
        cout << "Matrix L" << endl << cameraMatrix_l << endl;
        cout << "Distortion L" << endl << distCoeffs_l << endl;
        cout << "Matrix R" << endl << cameraMatrix_r << endl;
        cout << "Distortion R" << endl << distCoeffs_r << endl;
        printf("\n");

        intr_good = false; // Just says not to rewrite the intrinsics

        intr.release();

    }

    if(skip_extrinsics){
        extr_good = false;
        return intr_good;
    }

    const long flags =
            CALIB_FIX_INTRINSIC;

    //Run extrinsics
    T = Mat::zeros(3, 1, CV_64F);
    T.at<double>(0, 0) = -0.08;

    R = Mat::eye(3, 3, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calc_board_corner_positions(cp.board_size, cp.square_size, objectPoints[0]);
    objectPoints[0][cp.board_size.width - 1].x = objectPoints[0][0].x + grid_width;
    objectPoints.resize(imagePointsOverlap_l.size(),objectPoints[0]);

    printf(COLOR_LIT_BLU "Calibrating Extrinsics\n" RESET_FONT);
    printf("%lu frames will be processed\n", imagePointsOverlap_l.size());

    reprojErr_ext = stereoCalibrate(objectPoints, imagePointsOverlap_l, imagePointsOverlap_r, cameraMatrix_l, distCoeffs_l, cameraMatrix_r,
                        distCoeffs_r, imageSize, R, T, noArray(), noArray(), noArray(), flags,
                        cv::TermCriteria(cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 30, 1e-6));

    cout << "R" << endl << R << endl;
    cout << "T" << endl << T << endl;
    printf("Re-projection error reported by stereoCalibrate: %lf\n", reprojErr_ext);

    // baseline for horizontal stereo
    double baseline_m = -T.at<double>(0,0);

    // check for vertical stereo
    if(fabs(T.at<double>(0,0))<fabs(T.at<double>(1,0))){
        is_vertical = true;
        baseline_m = -T.at<double>(1,0);
        printf("Detected vertical stereo pair\n");
    }
    else{
        is_vertical = false;
        printf("Detected horizontal stereo pair\n");
    }

    printf("Distance between cameras: %0.4f\n", baseline_m);

    if(reprojErr_ext > EXTR_SUCCESS_CUTOFF){
        printf("Extrinsics Calibration " COLOR_RED "Failed\n" RESET_FONT);
        printf("exceeded allowable max reprojection error: %4.1lf\n", EXTR_SUCCESS_CUTOFF);
        extr_good = 0;
    }
    else if(baseline_m<=0.0 && !is_vertical){
        // only check for horizontal pairs. TODO when we decide on a convention
        // for vertical pairs, also check the baseline is correct here
        printf("Extrinsics Calibration " COLOR_RED "Failed\n" RESET_FONT);
        printf("left/right cameras appear to be reversed\n");
        extr_good = 0;
    }
    else{
        printf("Extrinsics Calibration " COLOR_GRN "Succeded!\n" RESET_FONT);
        extr_good = 1;
    }
    printf("\n");
    // return success if both succeded
    return extr_good & intr_good;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Saving mono or stereo calibrations in modalai format
/////////////////////////////////////////////////////////////////////////////////////////
static void write_date(FileStorage fs)
{

    char datestring[128];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(datestring,"%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    fs << "calibration_time" << datestring;

}

// Helper function to strip known tags
string strip_tags(const string &input_pipe) {
    // Define a regex to match known tags
    regex tag_pattern("(_misp|_grey|_color|_norm).*");
    return regex_replace(input_pipe, tag_pattern, "");
}

static void save_camera_params_mono(calib_params cp, Mat cameraMatrix, Mat distCoeffs, double reprojErr)
{
    char fileName[128];

    // Convert input_pipe to std::string for processing
    string input_pipe_str(input_pipe);
    // Strip tags from input_pipe
    string base_input_name = strip_tags(input_pipe_str);

    // Convert the stripped base name back to a char array
    char base_input_name_c[MODAL_PIPE_MAX_DIR_LEN];
    strncpy(base_input_name_c, base_input_name.c_str(), MODAL_PIPE_MAX_DIR_LEN - 1);
    base_input_name_c[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0'; // Ensure null-termination

    // Use the stripped name for the file
    sprintf(fileName, "%sopencv_%s_intrinsics.yml", DATA_ROOT, base_input_name_c);
    FileStorage fs(fileName, FileStorage::WRITE);

    printf("\nWriting data to: %s\n", fileName);

    fs << "M" << cameraMatrix;
    fs << "D" << distCoeffs;
    fs << "reprojection_error" << reprojErr;
    fs << "width" << imageSize.width;
    fs << "height" << imageSize.height;
    if (cp.use_fisheye) {
        fs << "distortion_model" << "fisheye";
    } else {
        fs << "distortion_model" << "plumb_bob";
    }
    write_date(fs);

    printf("Saved!\n");
}

static void save_camera_params_stereo(calib_params cp, Mat cameraMatrix_l, Mat cameraMatrix_r, Mat distCoeffs_l, Mat distCoeffs_r,
                                      Mat R, Mat T, double reprojErr_l, double reprojErr_r, double reprojErr_ext)
{
    char fileName[128];

    // Convert input_pipe to std::string for processing
    string input_pipe_str(input_pipe);
    // Strip tags from input_pipe
    string base_input_name = strip_tags(input_pipe_str);

    // Convert the stripped base name back to a char array if needed
    char base_input_name_c[MODAL_PIPE_MAX_DIR_LEN];
    strncpy(base_input_name_c, base_input_name.c_str(), MODAL_PIPE_MAX_DIR_LEN - 1);
    base_input_name_c[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0'; // Ensure null-termination

    if (intr_good) {
        sprintf(fileName, "%sopencv_%s_intrinsics.yml", DATA_ROOT, base_input_name_c);
        FileStorage intr(fileName, FileStorage::WRITE);

        intr << "M1" << cameraMatrix_l;
        intr << "D1" << distCoeffs_l;
        intr << "reprojection_error1" << reprojErr_l;
        intr << "M2" << cameraMatrix_r;
        intr << "D2" << distCoeffs_r;
        intr << "reprojection_error2" << reprojErr_r;
        intr << "width" << imageSize.width;
        intr << "height" << imageSize.height;
        if (cp.use_fisheye) {
            intr << "distortion_model" << "fisheye";
        } else {
            intr << "distortion_model" << "plumb_bob";
        }
        write_date(intr);
        intr.release();

        printf("Saved intrinsics to: %s\n", fileName);
    }

    if (extr_good) {
        sprintf(fileName, "%sopencv_%s_extrinsics.yml", DATA_ROOT, base_input_name_c);
        FileStorage extr(fileName, FileStorage::WRITE);

        extr << "R" << R;
        extr << "T" << T;
        extr << "reprojection_error" << reprojErr_ext;
        if (is_vertical) {
            extr << "orientation" << "horizontal";
        } else {
            extr << "orientation" << "vertical";
        }
        write_date(extr);
        extr.release();

        printf("Saved extrinsics to: %s\n", fileName);
    }
}
bool run_mono_calibration_and_save(calib_params cp, float grid_width, vector<vector<Point2f>> imagePoints)
{
    vector<Mat> rvecs, tvecs;

    Mat cameraMatrix, distCoeffs;
    double error;
    bool ok = run_calibration_mono(cp, rvecs, tvecs, grid_width, imagePoints, cameraMatrix, distCoeffs, error);

    if(ok){
        save_camera_params_mono(cp, cameraMatrix, distCoeffs, error);
    }
    set_result(ok);
    return ok;
}

bool run_stereo_calibration_and_save(calib_params cp, float grid_width, vector<vector<Point2f>>* imagePoints)
{
    vector<Mat> rvecs_l, rvecs_r, tvecs_l, tvecs_r;
    Mat cameraMatrix_l, cameraMatrix_r, distCoeffs_l, distCoeffs_r, R, T;
    double errorL, errorR, errorExt;

    int ok = run_calibration_stereo(cp, rvecs_l, rvecs_r, tvecs_l, tvecs_r, cameraMatrix_l, cameraMatrix_r,
                                     distCoeffs_l, distCoeffs_r, grid_width, R, T, imagePoints,
                                     errorL, errorR, errorExt);

    // this might save only extrinsics or intrinsics depending on if one was manually disabled
    save_camera_params_stereo(cp, cameraMatrix_l, cameraMatrix_r, distCoeffs_l, distCoeffs_r,
                                R, T, errorL, errorR, errorExt);
    set_result(ok);
    return true;
}


// used for thermal images, rescale a 16-bit image to 8 bits
static void _convert_16_to_8(uint16_t* in, char* out, int width, int height)
{
    // find min/max values in 16-bit range to scale by
    int min = 65535;
    int max = 0;
    int frame_size = width*height;

    for(int i=0; i<frame_size; i++){
        uint16_t value = in[i];

        if(value==0) continue;
        if(value==65535) continue;

        if(value > max) {
            max = value;
        }
        if(value < min) {
            min = value;
        }
    }
    //printf("min: %3d  max: %3d\n", min, max);

    if(min>=max){
        fprintf(stderr, "failed to find valid image data in RAW16 image\n");
        return;
    }

    float diff = max - min;
    float scale = 255/diff;

    // generate 8-bit image by scaling the 16-bit dynamic range
    for(int i=0; i<frame_size; i++){

        uint16_t value = in[i];
        out[i] = (value - min) * scale;
    }

}



/////////////////////////////////////////////////////////////////////////////////////////
// Thread functions for consuming camera frames and passing the to the processor funcs
/////////////////////////////////////////////////////////////////////////////////////////

// used to tell the sumer thread to shut down when exiting
void signal_consumer(void)
{
    pthread_mutex_unlock(&consumer_mutex);
    pthread_cond_signal(&consumer_cond);
}

// camera helper callback whenever a frame arrives
static void _helper_cb(int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context){

	if(!main_running) return;

    if(!pthread_mutex_trylock(&consumer_mutex)){

        if(consumer_frame == NULL){
            cp.is_mono = (meta.format != IMAGE_FORMAT_STEREO_RAW8) &&
                         (meta.format != IMAGE_FORMAT_STEREO_NV21) &&
                         (meta.format != IMAGE_FORMAT_STEREO_NV12);

            if(meta.format == IMAGE_FORMAT_STEREO_NV21 ||
               meta.format == IMAGE_FORMAT_NV21 ||
               meta.format == IMAGE_FORMAT_STEREO_NV12 ||
               meta.format == IMAGE_FORMAT_NV12){
                consumer_frame = (char *)malloc(meta.size_bytes/1.5);
            }
            else if(meta.format == IMAGE_FORMAT_STEREO_RAW8 ||
                    meta.format == IMAGE_FORMAT_RAW8){
                consumer_frame = (char *)malloc(meta.size_bytes);
            }
            else if(meta.format == IMAGE_FORMAT_RAW16) {
                // we are going to convert this to 8-bit shortly
                consumer_frame = (char *)malloc(meta.width*meta.height);
            } else {
                printf("\nERROR: Received invalid camera format %d\n", meta.format);
                if(meta.format==IMAGE_FORMAT_RGB){
                    printf("if you are trying to calibrate a lepton, use the raw8 stream, not the color stream\n");
                }
                main_running = 0;
                pipe_client_close(ch);
                pthread_mutex_unlock(&consumer_mutex);
                pthread_cond_signal(&consumer_cond);
                return;
            }
        }

        if(meta.format == IMAGE_FORMAT_STEREO_NV21 ||
           meta.format == IMAGE_FORMAT_STEREO_NV12){
            memcpy(consumer_frame, frame, meta.size_bytes/2/1.5);
            memcpy(consumer_frame+(int)(meta.size_bytes/2/1.5), frame+(int)(meta.size_bytes/2), meta.size_bytes/2/1.5);
        }
        else if(meta.format == IMAGE_FORMAT_NV21 ||
                  meta.format == IMAGE_FORMAT_NV12){
            memcpy(consumer_frame, frame, meta.size_bytes/1.5);
        }
        else if(meta.format == IMAGE_FORMAT_RAW16){
            _convert_16_to_8((uint16_t*)frame, consumer_frame, meta.width, meta.height);
        }
        else {
            memcpy(consumer_frame, frame, meta.size_bytes);
        }

        consumer_meta = meta;

        pthread_mutex_unlock(&consumer_mutex);
        pthread_cond_signal(&consumer_cond);
    }
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    fprintf(stderr, "Camera server disconnected, exiting\n");
    main_running = false;
    return;
}

static bool doneSampling = false;
bool isDoneSampling(){
    return doneSampling;
}

static bool stop_parsing = false;
static void *stdin_parser(__attribute__((unused)) void* data){

    int stdinFlags = fcntl(STDIN_FILENO, F_GETFL);
    //Set read to be non-blocking
    fcntl(STDIN_FILENO, F_SETFL, stdinFlags | O_NONBLOCK);

    char buf[20];

    while(!stop_parsing && main_running){

        int numread = read(STDIN_FILENO, buf, 15);

        if(numread > 0){
            if(numread == 1){
                printf("Taking Frame\n");
                capture_frame = true;
            } else if (!strcmp(buf, "stop\n")) {
                printf("Stopping sampling\n");
                finish_sampling = true;
            } else {
                buf[numread-1] = 0;
                printf("Invalid entry: \"%s\", please enter either an empty line to take a sample or \"stop\" to finish sampling\n", buf);
            }
        }

        sleep(1);

    }

    fcntl(STDIN_FILENO, F_SETFL, stdinFlags);
    return NULL;

}
//Return data on success, null on failure
void *ConsumerThreadFunc(void *data) {

    Mat padded;

    //Setup the frame consumer thread (need to do this because we process frames very slowly and we need to not fill the pipe)
    pthread_condattr_t condAttr;
    pthread_condattr_init(&condAttr);
    pthread_condattr_setclock(&condAttr, CLOCK_MONOTONIC);
    pthread_cond_init(&consumer_cond, &condAttr);
    pthread_mutex_init(&consumer_mutex, NULL);
    pthread_mutex_lock(&consumer_mutex);

    // set up all our MPA callbacks
    pipe_client_set_camera_helper_cb(CAMERA_CH, _helper_cb, NULL);
    pipe_client_set_disconnect_cb(CAMERA_CH, _disconnect_cb, NULL);

    int ret = pipe_client_open(CAMERA_CH, input_pipe, PROCESS_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

    // check for errors trying to connect to the server pipe
    if(ret<0){
        pipe_print_error(ret);
        return (void*)-1;
    }


    vector<vector<Point2f>>* imagePoints = (vector<vector<Point2f>>*)data;
    camera_image_metadata_t& meta = consumer_meta;

    pthread_t parserThread;
    if (no_target_extrinsics) {
        pthread_attr_t parserAttr;
        pthread_attr_init(&parserAttr);
        pthread_attr_setdetachstate(&parserAttr, PTHREAD_CREATE_JOINABLE);
        pthread_create(&parserThread, &parserAttr, stdin_parser, NULL);
        pthread_attr_destroy(&parserAttr);
    }

    bool sigrec = false;

    while(main_running){

        if (!sigrec){
            printf("Waiting for valid pipe...\n");
        }

        pthread_cond_wait(&consumer_cond, &consumer_mutex);
        if(!main_running) break; //Signal handler told us to stop

        if (!sigrec){
            printf("Please open voxl-portal in a web browser to view the camera calibrator overlay stream\n");
        }

        sigrec = true;
        static int h_raw = meta.height;
        static int w_raw = meta.width;

        imageSize = Size(meta.width, meta.height);
        
        if(!doneSampling){//Need more samples

            if (cp.is_mono){
                doneSampling = process_mono_frame(cp, meta, consumer_frame, imagePoints[0]);
            } else {
                doneSampling = process_stereo_frame(cp, meta, consumer_frame, imagePoints);
            }

            if(doneSampling){
                stop_parsing = true;
                running_extrinsics = false;
            }

        } else {//Done getting images, make a pretty overlay

            if (cp.is_mono){

                Mat mono_raw(h_raw, w_raw, CV_8UC1, consumer_frame);

                Mat mono_color;
                Mat in[] = {mono_raw, mono_raw, mono_raw};
                merge(in, 3, mono_color);


                for(vector<Point2f> board : imagePoints[0]){
                    drawChessboardCorners(mono_color, cp.board_size, Mat(board), true);
                }

                if(mirror_image){
                    flip(mono_color, mono_color, 1);
                }

                padded = _make_padded_overlay_mono(mono_color, 1.0);
            } else {
                Mat left_raw(h_raw, w_raw, CV_8UC1, consumer_frame),
                    right_raw(h_raw, w_raw, CV_8UC1, &consumer_frame[w_raw*h_raw]);

                Mat left_color;
                cv::Mat in_left[] = {left_raw, left_raw, left_raw};
                cv::merge(in_left, 3, left_color);
                Mat right_color;
                cv::Mat in_right[] = {right_raw, right_raw, right_raw};
                cv::merge(in_right, 3, right_color);

                for(vector<Point2f> board : imagePoints[2]){
                    drawChessboardCorners(left_color, cp.board_size, Mat(board), true);
                }
                for(vector<Point2f> board : imagePoints[3]){
                    drawChessboardCorners(right_color, cp.board_size, Mat(board), true);
                }
                for(vector<Point2f> board : imagePoints[0]){
                    drawChessboardCorners(left_color, cp.board_size, Mat(board), true);
                }
                for(vector<Point2f> board : imagePoints[1]){
                    drawChessboardCorners(right_color, cp.board_size, Mat(board), true);
                }

                if(mirror_image){
                    flip(left_color, left_color, 1);
                    flip(right_color, right_color, 1);
                }

                padded = _make_padded_overlay_stereo(left_color, right_color, 1);

            }

            // write to the overlay
            if(!finished_calibrating){
                putText(padded, "Finished Sampling, Running Calibration...", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, BLACK, 3, LINE_AA);
                putText(padded, "Finished Sampling, Running Calibration...", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, WHITE, 1, LINE_AA);
            }
            else{
                if(calibration_passed){
                    putText(padded, "Calibration PASSED!", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, BLACK, 3, LINE_AA);
                    putText(padded, "Calibration PASSED!", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, GREEN, 1, LINE_AA);
                }
                else{
                    putText(padded, "Calibration FAILED, please try again.", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, WHITE, 3, LINE_AA);
                    putText(padded, "Calibration FAILED, please try again.", Point(100, 150), FONT_HERSHEY_SIMPLEX, 0.75, RED,   1, LINE_AA);
                }
            }
            camera_image_metadata_t overlay_meta = meta;
            overlay_meta.format = IMAGE_FORMAT_RGB;
            overlay_meta.width  = padded.cols;
            overlay_meta.stride = padded.cols * 3;
            overlay_meta.height = padded.rows;
            overlay_meta.size_bytes = padded.rows * padded.cols * 3;

            // send the final "finished" frame
            // this will loop through a handful of times
            pipe_server_write_camera_frame(OVERLAY_CH, overlay_meta, (char*)padded.data);
            usleep(100000); // Don't need this too frequenstly
        }

    }

    pthread_mutex_unlock(&consumer_mutex);

    if (no_target_extrinsics){
        printf("waiting for parser thread to join\n");
        pthread_join(parserThread, NULL);
    }

    printf("Exiting Cleanly\n");

    pipe_client_close_all();
    pipe_server_close_all();

    return NULL;

}
